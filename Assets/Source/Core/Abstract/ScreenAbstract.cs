﻿namespace Developer.Core
{
    /// <inheritdoc />
    /// <summary>
    /// Represents a screen that can be loaded in the menu system of the app.
    /// </summary>
    public abstract class ScreenAbstract : UnityEngine.MonoBehaviour
    {
        /// <summary>
        /// A reference back to the <see cref="App"/> that is running the application.
        /// </summary>
        public App _app { get; set; } = null;
    }
}