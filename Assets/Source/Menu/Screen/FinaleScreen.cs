﻿//TODO: Complete this screen

namespace Developer.Screen
{
	using UnityEngine;
	using UnityEngine.UI;
	/// <inheritdoc />
	/// <summary>
	/// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
	/// </summary>
	public sealed class FinaleScreen : Core.ScreenAbstract
	{
		private Button myButton;
		
		private void Awake()
		{
			myButton = FindObjectOfType<Button>();

			if (Application.platform == RuntimePlatform.Android)
			{
				gameObject.GetComponent<Image>().color = Color.red;

			}
			else if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				gameObject.GetComponent<Image>().color = Color.red;

			}
			else if (Application.platform == RuntimePlatform.WindowsEditor)
			{
				gameObject.GetComponent<Image>().color = Color.yellow;

			}

			if (Screen.orientation == ScreenOrientation.Portrait)
			{
				myButton.transform.position = new Vector3 (Screen.width / 2, Screen.height /2, gameObject.transform.position.y);

			}
			else if (Screen	.orientation == ScreenOrientation.Landscape)
			{
				myButton.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, gameObject.transform.position.y); //This is the same thing, come back to this

			}
		}

		private void Update()
		{
			myButton.onClick.AddListener(ClickEvent);
				
		}

		public void ClickEvent()
		{
			_app.ChangeScreen<Developer.Screen.LoadingScreen>();
		}
	}
}