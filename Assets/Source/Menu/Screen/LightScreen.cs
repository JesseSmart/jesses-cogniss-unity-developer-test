﻿namespace Developer.Screen
{
	using UnityEngine;
	using Test;
	using UnityEngine.UI;
	/// <inheritdoc />
	/// <summary>
	/// Controller for the screen that displays a <see cref="Developer.Component.TrafficLight" />.
	/// </summary>
	public sealed class LightScreen : Core.ScreenAbstract
	{
		private Developer.Component.TrafficLight lights = null;

		private float timer;
		private float duration = 3;
		private int index = 1;
		private int repetitions = 100; //lessen this to shortcut the wait before the finale screen
		private Text timerText;

		private void Awake()
		{
			timerText = GetComponentInChildren<Text>();
			timer = duration;
			GameObject trafficLightPrefab = Resources.Load<GameObject>("Prefabs/Component/TrafficLight/TrafficLight");
			GameObject trafficLightInstance = Instantiate(trafficLightPrefab, transform.Find("TrafficLightHolder"));
			lights = trafficLightInstance.AddComponent<Developer.Component.TrafficLight>();
			lights.SetState();
		}

		//TODO: According the rules, turn the lights on and off...
		//Hint: The networking functionality is inside the "Test" namespace.
		private void Update()
		{
			timer -= Time.deltaTime;
			timerText.text = "Timer: : " + timer.ToString("0.00") + " | Index: " + (index - 1);
			if (timer <= 0 && index <= repetitions)
			{
				Lighter(index);
				index++;
				timer = duration;

			}
			else if (index > repetitions)
			{
				_app.ChangeScreen<FinaleScreen>();

			}
		}


		void Lighter(int ind)
		{
			print("Light Index = " + ind);
			float thirdRem = ind % 3f;
			float fifthRem = ind % 5f;

			if (thirdRem == 0 && fifthRem == 0)
			{
				lights.SetState(LightColor.Green);
			}
			else if (thirdRem == 0)
			{
				lights.SetState(LightColor.Red);
			}
			else if (fifthRem == 0)
			{
				lights.SetState(LightColor.Yellow);
			}
			else
			{
				lights.SetState();
			}

		}
	}

	//IEnumurator Wait ()
	//{
	//	yield return new WaitForSeconds(3f);
	//	Lighter()
	//}
	
}


