﻿namespace Developer.Component
{
    using UnityEngine;
    using UnityEngine.UI;

    /// <inheritdoc />
    /// <summary>
    /// Controller for a Traffic Light.
    /// </summary>
    public sealed class TrafficLight : MonoBehaviour
    {
        /// <summary>
        /// The light representing Stop.
        /// </summary>
        private Image red = null;

        /// <summary>
        /// The light representing Caution.
        /// </summary>
        private Image yellow = null;

        /// <summary>
        /// The light representing Go.
        /// </summary>
        private Image green = null;

        private void Awake()
        {
            red = transform.Find("Red").GetComponent<Image>();
            yellow = transform.Find("Yellow").GetComponent<Image>();
            green = transform.Find("Green").GetComponent<Image>();
        }

        /// <summary>
        /// Turns off all lights.
        /// </summary>
        public void SetState()
        {
            if (red == null || yellow == null || green == null) Debug.LogError($"TrafficLight - SetState(): TrafficLight has a null reference.");

            red.color = Color.gray;
            yellow.color = Color.gray;
            green.color = Color.gray;
        }

        /// <summary>
        /// Sets the selected light on, while turning the other lights off.
        /// </summary>
        /// <param name="lightColor">The light to switch on.</param>
        public void SetState(Test.LightColor lightColor)
        {
            if (red == null || yellow == null || green == null) Debug.LogError($"TrafficLight - SetState(light): TrafficLight has a null reference.");

            switch (lightColor)
            {
                case Test.LightColor.Red:
                    red.color = Color.red;
                    yellow.color = Color.gray;
                    green.color = Color.gray;
                    break;
                case Test.LightColor.Yellow:
                    red.color = Color.gray;
                    yellow.color = Color.yellow;  //This said grey before which ment the yellow light did't turn on - Jesse
                    green.color = Color.gray;
                    break;
                case Test.LightColor.Green:
                    red.color = Color.gray;
                    yellow.color = Color.gray;
                    green.color = Color.green;
                    break;
                default:
                    Debug.LogError($"TrafficLight - SetState(light): Attempted to set state on non-existent index {lightColor}.");
                    break;
            }
        }
    }
}